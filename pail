#!/bin/sh
set -e

# path to pail directory
dir="$(dirname "$(realpath "$0")")"

color='\033[0;35m'
bold='\033[;1m'
nc='\033[0m'    # no color

if [ "$USER" = "root" ]; then
	# I don't trust my own code
	echo
	echo "    Yikes! Please don't run pail as root."
	exit 1
fi

pail_installed=false
case "$PATH" in
	*"pail"*)
		pail_installed=true
		;;
	*"${HOME}/.local/bin"*)
		;;
	*)
		echo "    ~/.local/bin doesn't seem to be in your \$PATH. Better add it."
		exit 1
		;;
esac

# echo with escape codes, POSIX-compliant
puts() {
	printf "%b\n" "$1"
}

pail_install() {
	# symlink this script under ~/.local/bin/
	ln -s "${dir}/pail" "${HOME}/.local/bin/pail"
	puts " ${color}::${nc} ${bold}Done!${nc}"
	stat "${HOME}/.local/bin/pail" | sed 's/^/    /'
	exit 0
}

pail_up() {
	# insert pail scripts into $PATH
	echo
	if [ $pail_installed = true ]; then
		echo "    Hmm, pail seems be in your \$PATH already."
		exit 1
	fi
	export PATH="${dir}:${PATH}"
	puts " ${color}::${nc} ${bold}The following pail scripts have been loaded:${nc}"
	find "${dir}" -maxdepth 1 -type f -executable -printf "    %f\n"
	echo
	puts " ${color}::${nc} ${bold}Spawning a new shell. Welcome to pail!${nc}"
	echo
	if [ "$SHELL" = "/bin/zsh" ]; then
		# nifty ZDOTDIR hack to load our own .zshrc
		if [ -n "$ZDOTDIR" ]; then
			export ZDOTDIR_OLD=${ZDOTDIR}
		fi
		export ZDOTDIR=${dir}
		/bin/zsh -i
	else
		# with sh we don't source the user's profile :( ... fix this?
		export PS1="${bold}\u${nc} ${color}+pail${nc} \w ${color}»${nc} "
		/bin/sh --rcfile /dev/null -i
	fi
	exit 0
}

case "$1" in
	-i|install)
		pail_install
		;;
	-u|up)
		pail_up
		;;
	-h|help|*)
		echo
		cat <<-HELP
		Pail is a collection of scripts designed to help in CTF competitions. It focuses
		mainly on providing helpful tools for forensics, reverse engineering, and binary
		exploitation. Have fun!

		Usage: pail [command]
		Commands:
		 -i, install                symlinks the pail executable under ~/.local/bin/
		 -u, up                     loads pail scripts into your \$PATH
		 -h, help                   print this help text to the console
		HELP
		;;
esac

