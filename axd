#!/bin/sh
set -e

file_in="/dev/stdin"
file_out="/dev/stdout"

dot_ascii() {
	# trim quote characters
	local ascii="${2%\"}"
	local ascii="${ascii#\"}"
	echo -n $ascii | xxd -p -c 256
}

dot_fill() {
	# trim comma
	local rep=${2%,}
	local hex=$3
	local i=0
	while [ $i -lt $rep ]; do
		echo -n $hex
		i=$((i+1))
	done
	echo
}

while getopts "hi:o:" arg; do
	case "$arg" in
		i)
			file_in=$OPTARG
			;;
		o)
			file_out=$OPTARG
			;;
		h|*)
			echo
			cat <<-HELP
			Sometimes, it's simpler to just work in hex. Use axd to manage annotated hexdump
			files, which I like to end with the .ax extension. All lines that don't start
			with a valid hex character [a-fA-F0-9] are stripped, and from there this is
			pretty much just a wrapper for xxd. This means that you can comment out lines
			however you like. Leading whitespace is ignored. Basic .directives are also
			supported, in the style of GNU AS.

			Usage: axd [-h] [-i IN] [-o OUT]
			Commands:
			 -h                     print this help text to the console
			 -i IN                  read from annotated hex file IN (defaults to stdin)
			 -o OUT                 write to binary file OUT (defaults to stdout)

			Directives:
			 .ascii "abc"           insert hex representation of "abc"
			 .fill 2, 90            insert 0x90 2 times
			 !rasm2 nop             run the command following the exclamation mark in your
						shell; in this example, call rasm2 to insert 0x90
			HELP
			exit 0
			;;
	esac
done

if [ -e /tmp/axd.x ]; then
    rm /tmp/axd.x
fi
touch /tmp/axd.x

while read -r line; do
	# strip leading whitespace
	line=$(echo "$line" | sed 's/^[[:space:]]//')
	# handle .directives
	case $line in
		.ascii*)
			line=$(dot_ascii $line)
			;;
		.fill*)
			line=$(dot_fill $line)
			;;
		!*)
			line=$(${SHELL} -c "${line:1}")
			;;
	esac
	# write to tmpfile
	echo "$line" >> "/tmp/axd.x"
done < "$file_in"

# clear file of non-hex lines
sed -i '/^[a-fA-F0-9]/!d' /tmp/axd.x

# xxd time
xxd -r -p /tmp/axd.x "$file_out"

exit 0

