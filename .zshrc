# source the user's .zshrc first
if [ -n "$ZDOTDIR_OLD" ]; then
    $ZDOTDIR=${ZDOTDIR_OLD}
    source $ZDOTDIR_OLD/.zshrc
else
    source $HOME/.zshrc
fi

# override prompt
PS1=$'\e[0;1m%n \e[0;35m+pail \e[0m%2~ \e[1;35m» \e[0m'

