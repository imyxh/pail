Pail
====

Pail is my collection of scripts for CTF competitions, focusing on binary 
exploitation, reverse engineering, and forensics.

Pail isn't a full-fledged framework yet, so this is how it works:

 0. clone this repository wherever you like;
 1. run `./pail install` to symlink the pail script into your ~/.local/bin;
 2. run `pail up` to open a shell with all the pail scripts in your $PATH.

There are many advantages of using this sort of model for a collection of 
scripts:

  - pail commands are only there when you want them,
  - installation and removal is hassle-free,
  - you can add new scripts to the repository very easily, and
  - you can have a pretty shell prompt to let you know when pail is loaded.

The main pail script should work with zsh and bash. I haven't tested this with 
other shells.

